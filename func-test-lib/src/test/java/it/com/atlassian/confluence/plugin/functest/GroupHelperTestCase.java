package it.com.atlassian.confluence.plugin.functest;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.GroupHelper;
import com.atlassian.confluence.plugin.functest.helper.UserHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GroupHelperTestCase extends AbstractConfluencePluginWebTestCase
{

    protected void createUser(UserHelper userHelper)
    {
        userHelper.setName("john.doe");
        userHelper.setFullName("John Doe");
        userHelper.setEmailAddress("john.doe@localhost.localdomain");
        userHelper.setPassword("foobar");
        userHelper.setGroups(Arrays.asList("confluence-users"));

        assertTrue(userHelper.create());
    }

    public void testCreateGroup()
    {
        final GroupHelper groupHelper = getGroupHelper();
        final UserHelper userHelper = getUserHelper();
        final List<String> groupNames;

        groupHelper.setName("test-group");
        assertTrue(groupHelper.create());

        createUser(userHelper);

        /* Ensure user is not a member of test-group */
        assertFalse(userHelper.getGroups().contains(groupHelper.getName()));

        /* Make the user a member of test-group */
        groupNames = new ArrayList<String>(userHelper.getGroups());
        groupNames.add(groupHelper.getName());
        userHelper.setGroups(groupNames);
        assertTrue(userHelper.update());

        assertTrue(userHelper.read());
        /* Ensure user is a member of test-group */
        assertTrue(userHelper.getGroups().contains(groupHelper.getName()));

        /* Remove test-group */
        assertTrue(groupHelper.delete());

        assertTrue(userHelper.read());
        /* Ensure user is not a member of test-group */
        assertFalse(userHelper.getGroups().contains(groupHelper.getName()));

        assertTrue(userHelper.delete());
    }
}
