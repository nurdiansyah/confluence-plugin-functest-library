package it.com.atlassian.confluence.plugin.functest.selenium;

import com.atlassian.confluence.plugin.functest.ConfluencePluginSeleniumTestCaseBase;
import com.atlassian.confluence.plugin.functest.util.ClasspathResourceUtil;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.plugin.functest.helper.HelperFactory;
import junit.framework.Assert;

import java.io.File;
import java.io.IOException;


public class SimpleSeleniumConfluencePluginSeleniumTestCase extends ConfluencePluginSeleniumTestCaseBase
{

    public void testDashboardAccessible()
    {
        tester.open(tester.getContextPath() + "/dashboard.action");
        tester.waitForPageToLoad();

        Assert.assertTrue(tester.isTextPresent("Dashboard"));
    }

    public void testFlushCache()
    {
        tester.flushCaches();
    }

    public void testSoapApiAccessible()
    {
        PageHelper pageHelper = HelperFactory.createPageHelper(tester);

        String spaceKey = "ds";
        String pageTitle = "testSoapApiAccessible";
        
        pageHelper.setSpaceKey(spaceKey);
        pageHelper.setTitle(pageTitle);
        pageHelper.setContent("Cheese");

        Assert.assertTrue(pageHelper.create());

        tester.open(tester.getContextPath() + "/display/" + spaceKey + "/" + pageTitle);
        Assert.assertTrue(tester.isTextPresent("Cheese"));
    }

    public void testXmlRpcApiAccessible()
    {
        String spaceKey = "ds";
        String pageTitle = "testSoapApiAccessible";

        testSoapApiAccessible();

        PageHelper pageHelper = HelperFactory.createPageHelper(tester);

        pageHelper.setSpaceKey(spaceKey);
        pageHelper.setTitle(pageTitle);

        // Search is done via xmlrpc
        Assert.assertTrue(pageHelper.findBySpaceKeyAndTitle() > 0);
    }

    public void testRestoreBackup() throws IOException
    {
        File siteExportContainingSeleniumSpace = ClasspathResourceUtil.getClassPathResourceAsTempFile(
                "site-export-containing-selenium-space.zip",
                getClass().getClassLoader(),
                ".zip"
        );

        tester.restoreData(siteExportContainingSeleniumSpace);
        tester.open(tester.getContextPath() + "/display/SS");

        Assert.assertTrue(tester.isTextPresent("Selenium Space"));
    }

    public void testInstallPlugin() throws IOException
    {
        File pageHistorySliderPlugin = ClasspathResourceUtil.getClassPathResourceAsTempFile("page-history-slider-1.0.jar",
                getClass().getClassLoader(), ".jar");

        tester.installPlugin(pageHistorySliderPlugin);
        try
        {
            tester.gotoPageWithEscalatedPrivileges("/admin/viewplugins.action");

            tester.waitForPageToLoad();

            Assert.assertTrue(tester.isTextPresent("Page History Slider"));
        }
        finally
        {
            tester.dropEscalatedPrivileges();
        }
    }
}
