package it.com.atlassian.confluence.plugin.functest;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;

import java.io.IOException;


public class UpdateLicenseTestCase extends AbstractConfluencePluginWebTestCase
{
    public void testUpdateLicenseEvaluationLicense() throws IOException
    {
        getConfluenceWebTester().setLicenseString(
                "rRRMwChVNpPLWSWktDxRQeNmnptqWJsFFflICceowOasig\n" +
                        "mi2KnDAA8Ed0NstOZJ4p9xqO2K16z7rimO8p9Ovu0ZKDjQ\n" +
                        "mqwWOQMRnPoNOMQoQVWNPpQOMrRmoNRrtuTtTxVwWXxMsv\n" +
                        "WXtuVxrWwwStsVouuunuXSUVqNppoUUnomsprsroqqqpUU\n" +
                        "nomsprsroqqqpUU1qiXppfXkWJlcqtXobWJvpqbjpWGqvW\n" +
                        "CqaUUrmm"
        ); /* Eval license */
        updateLicense();
    }
}
