package it.com.atlassian.confluence.plugin.functest;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.BlogPostHelper;
import com.atlassian.confluence.plugin.functest.helper.CommentHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.plugin.functest.util.ClasspathResourceUtil;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class CommentHelperTestCase extends AbstractConfluencePluginWebTestCase
{
    private File demoSiteExport;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        demoSiteExport = ClasspathResourceUtil.getClassPathResourceAsTempFile("confluence-demo-site.zip", getClass().getClassLoader(), ".zip");
        getConfluenceWebTester().restoreData(demoSiteExport);
    }

    @Override
    protected void tearDown() throws Exception
    {
        if (demoSiteExport.isFile())
            demoSiteExport.delete();
        super.tearDown();
    }

    public void testCreateCommentOnBlog() throws ParseException
    {
        final BlogPostHelper blogPostHelper = getBlogPostHelper();
        final CommentHelper commentHelper = getCommentHelper();
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        final long octagonBlogpostId;

        blogPostHelper.setSpaceKey("ds");
        blogPostHelper.setTitle("Octagon blog post");
        blogPostHelper.setCreationDate(simpleDateFormat.parse("2004/11/21"));

        assertTrue(0 < (octagonBlogpostId = blogPostHelper.findBySpaceKeyPublishedDateAndBlogPostTitle()));

        commentHelper.setContentId(octagonBlogpostId);
        commentHelper.setContent("Wonderful blog!");

        assertTrue(commentHelper.create());
        assertTrue(0 < commentHelper.getId());

        gotoPage("/display/ds/" + simpleDateFormat.format(blogPostHelper.getCreationDate()) + "/Octagon+blog+post?showComments=true&focusedCommentId=" + commentHelper.getId());
        assertTextPresent("Wonderful blog!");

        assertTrue(commentHelper.delete());
    }

    public void testCreateReplyComment()
    {
        final PageHelper pageHelper = getPageHelper();
        final long confluenceOverviewPageId;
        final long parentCommentId;
        final CommentHelper anotherCommentHelper;

        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("Confluence Overview");

        assertTrue(0 < (confluenceOverviewPageId = pageHelper.findBySpaceKeyAndTitle()));

        String parentCommentContent = "parentCommentContent";
        CommentHelper parentComment = getCommentHelper();
        parentComment.setContent(parentCommentContent);
        parentComment.setContentId(confluenceOverviewPageId);
        assertTrue(parentComment.create());

        pageHelper.setId(confluenceOverviewPageId);
        assertTrue(pageHelper.getCommentIds().size() > 0);

        parentCommentId = parentComment.getId();

        final CommentHelper childComment = getCommentHelper();
        String childCommentContent = "childCommentContent";
        childComment.setParentId(parentComment.getId());
        childComment.setContentId(pageHelper.getId());
        childComment.setContent(childCommentContent);

        assertTrue(childComment.create());
        assertTrue(0 < childComment.getId());

        gotoPage("/display/ds/Confluence+Overview?showComments=true&focusedCommentId=" + childComment.getId());
        assertTextPresent(parentCommentContent);
        assertTextPresent(childCommentContent);

        anotherCommentHelper = getCommentHelper(childComment.getId());
        assertTrue(anotherCommentHelper.read());
        assertEquals(parentCommentId, anotherCommentHelper.getParentId());

        assertTrue(childComment.delete());
    }

    /*
     * Method temporarily commented out until Confluence's remote API allows us to edit comments. 
     */
//    public void testEditComment() {
//        final PageHelper pageHelper = getPageHelper();
//        final long confluenceOverviewPageId;
//        final CommentHelper commentHelper = getCommentHelper();
//        final long commentId;
//
//        pageHelper.setSpaceKey("ds");
//        pageHelper.setTitle("Confluence Overview");
//
//        assertTrue(0 < (confluenceOverviewPageId = pageHelper.findBySpaceKeyAndTitle()));
//
//        commentHelper.setContentId(confluenceOverviewPageId);
//        commentHelper.setContent("Everything can be searched in Confluence! Woohoo!");
//
//        assertTrue(commentHelper.create());
//        assertTrue(0 < (commentId = commentHelper.getId()));
//
//        gotoPage("/display/ds/Confluence+Overview?showComments=true&focusedCommentId=" + commentHelper.getId());
//        assertTextPresent("Everything can be searched in Confluence! Woohoo!");
//
//        commentHelper.setContent("Confluence is ultra searchable!");
//        assertTrue(commentHelper.updateMailServer());
//        assertEquals(commentId, commentHelper.getId());
//
//        gotoPage("/display/ds/Confluence+Overview?showComments=true&focusedCommentId=" + commentHelper.getId());
//        assertTextNotPresent("Everything can be searched in Confluence! Woohoo!");
//        assertTextNotPresent("Confluence is ultra searchable!");
//
//        assertTrue(commentHelper.delete());
//    }

    public void testReadComment()
    {
        final PageHelper pageHelper = getPageHelper();
        final long confluenceOverviewPageId;
        final CommentHelper commentHelper = getCommentHelper();
        final long commentId;
        final CommentHelper anotherCommentHelper;

        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("Confluence Overview");

        assertTrue(0 < (confluenceOverviewPageId = pageHelper.findBySpaceKeyAndTitle()));

        commentHelper.setContentId(confluenceOverviewPageId);
        commentHelper.setContent("Everything can be searched in Confluence! Woohoo!");

        assertTrue(commentHelper.create());
        assertTrue(0 < (commentId = commentHelper.getId()));

        anotherCommentHelper = getCommentHelper(commentId);
        assertTrue(anotherCommentHelper.read());

        assertEquals(confluenceOverviewPageId, anotherCommentHelper.getContentId());
        assertEquals(commentId, anotherCommentHelper.getId());
        assertEquals("<p>Everything can be searched in Confluence! Woohoo!</p>", anotherCommentHelper.getContent());
        assertEquals(getConfluenceWebTester().getCurrentUserName(), anotherCommentHelper.getCreator());

        assertTrue(commentHelper.delete());
    }
}
