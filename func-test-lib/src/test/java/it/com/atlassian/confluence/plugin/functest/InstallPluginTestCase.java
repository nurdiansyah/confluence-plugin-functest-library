package it.com.atlassian.confluence.plugin.functest;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.util.ClasspathResourceUtil;
import com.atlassian.confluence.plugin.functest.util.PluginMetadataUtil;
import junit.framework.Assert;

import java.io.File;

public class InstallPluginTestCase extends AbstractConfluencePluginWebTestCase
{

    private File pluginFile;

    protected void setUp() throws Exception
    {
        super.setUp();
        pluginFile = ClasspathResourceUtil.getClassPathResourceAsTempFile("themes-topandleftnavigation.jar", getClass().getClassLoader(), ".jar");
    }

    protected void tearDown() throws Exception
    {
        if (null != pluginFile)
            assertTrue(pluginFile.delete());

        super.tearDown();
    }
    
    public void testInstallPlugin()
    {
        getConfluenceWebTester().installPlugin(pluginFile);

        try
        {
            getConfluenceWebTester().gotoPageWithEscalatedPrivileges("/admin/viewplugins.action");

            String pluginName = PluginMetadataUtil.getPluginName(PluginMetadataUtil.getPluginDescriptorDom(pluginFile));

            assertLinkPresentWithText(pluginName);
            clickLinkWithText("Uninstall plugin");
        }
        catch (Exception e)
        {
            Assert.fail("Unable to get plugin name from plugin JAR at " + pluginFile);
        }
        finally
        {
            getConfluenceWebTester().dropEscalatedPrivileges();
        }
    }
}
