package it.com.atlassian.confluence.plugin.functest;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.util.ClasspathResourceUtil;

import java.io.File;

public class DataRestoreTestCase extends AbstractConfluencePluginWebTestCase
{
    private File backupFile;

    protected void setUp() throws Exception
    {
        super.setUp();
        backupFile = ClasspathResourceUtil.getClassPathResourceAsTempFile("confluence-demo-site.zip", getClass().getClassLoader(), ".zip");
    }

    protected void tearDown() throws Exception
    {
        if (null != backupFile)
            assertTrue(backupFile.delete());

        super.tearDown();
    }

    public void testRestoreData()
    {
        gotoPage("/dashboard.action");
        assertLinkPresentWithText("Demonstration Space");

        assertTrue(getSpaceHelper("ds").delete());

        gotoPage("/dashboard.action");
        assertLinkNotPresentWithText("Demonstration Space");

        getConfluenceWebTester().restoreData(backupFile);

        gotoPage("/dashboard.action");
        assertLinkPresentWithText("Demonstration Space");
    }
}
