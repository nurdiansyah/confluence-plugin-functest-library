package it.com.atlassian.confluence.plugin.functest;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.MailServerHelper;
import org.apache.commons.lang.StringUtils;
import org.xml.sax.SAXException;

import java.util.List;
import java.util.Map;

public class MailServerHelperTestCase extends AbstractConfluencePluginWebTestCase
{

    private void createMailServer(
            final MailServerHelper mailServerHelper,
            final String mailServerName,
            final String mailServerFromAddress,
            final String mailPrefix,
            final String mailServerHostName)
    {
        mailServerHelper.setName(mailServerName);
        mailServerHelper.setFromAddress(mailServerFromAddress);
        mailServerHelper.setPrefix(mailPrefix);

        mailServerHelper.setAddress(mailServerHostName);
        mailServerHelper.setUserName(getConfluenceWebTester().getCurrentUserName());
        mailServerHelper.setPassword(getConfluenceWebTester().getCurrentPassword());

        assertTrue(mailServerHelper.create());
    }

    private void assertMailServer(final MailServerHelper mailServerHelper) throws SAXException
    {
        try
        {
            getConfluenceWebTester().gotoPageWithEscalatedPrivileges("/admin/mail/editsmtpmailserver.action?id=" + mailServerHelper.getId());

            assertEquals(StringUtils.defaultString(mailServerHelper.getName()), getElementAttributByXPath("//form[@name='editsmtpserver']//input[@name='name']", "value"));
            assertEquals(StringUtils.defaultString(mailServerHelper.getFromAddress()), getElementAttributByXPath("//form[@name='editsmtpserver']//input[@name='from']", "value"));
            assertEquals(StringUtils.defaultString(mailServerHelper.getPrefix()), getElementAttributByXPath("//form[@name='editsmtpserver']//input[@name='prefix']", "value"));
            assertEquals(StringUtils.defaultString(mailServerHelper.getAddress()), getElementAttributByXPath("//form[@name='editsmtpserver']//input[@name='hostname']", "value"));
            assertEquals(StringUtils.defaultString(mailServerHelper.getUserName()), getElementAttributByXPath("//form[@name='editsmtpserver']//input[@name='userName']", "value"));

            /* Ignored this assertion - it seems that since Confluence 3.5, the mail server password is prepopulated with the SMTP user password. Bad idea, if you ask me, but what the hell */
//            assertEquals(StringUtils.EMPTY, getElementAttributByXPath("//form[@name='editsmtpserver']//input[@name='password']", "value"));
            assertEquals(StringUtils.defaultString(mailServerHelper.getJndiLocation()), getElementAttributByXPath("//form[@name='editsmtpserver']//input[@name='jndiName']", "value"));
        }
        finally
        {
            getConfluenceWebTester().dropEscalatedPrivileges();
        }
    }

    public void testCreateMailServer() throws SAXException
    {
        final MailServerHelper mailServerHelper = getMailServerHelper();

        String mailServerName = "Test Mail Server";
        String mailServerFromAddress = "admin@localhost.localdomain";
        String mailPrefix = "[Test Prefix]";
        String mailServerHostName = "localhost.localdomain";

        createMailServer(mailServerHelper, mailServerName, mailServerFromAddress, mailPrefix, mailServerHostName);

        assertMailServer(mailServerHelper);

        assertTrue(mailServerHelper.delete());
    }

    public void testReadMailServer()
    {
        final MailServerHelper mailServerHelper = getMailServerHelper();
        final MailServerHelper anotherMailServerHelper;

        String mailServerName = "Test Mail Server";
        String mailServerFromAddress = "admin@localhost.localdomain";
        String mailPrefix = "[Test Prefix]";
        String mailServerHostName = "localhost.localdomain";

        createMailServer(mailServerHelper, mailServerName, mailServerFromAddress, mailPrefix, mailServerHostName);


        anotherMailServerHelper = getMailServerHelper(mailServerHelper.getId());

        assertTrue(anotherMailServerHelper.read());

        assertEquals(mailServerHelper.getId(), anotherMailServerHelper.getId());
        assertEquals(mailServerHelper.getName(), anotherMailServerHelper.getName());
        assertEquals(mailServerHelper.getFromAddress(), anotherMailServerHelper.getFromAddress());
        assertEquals(mailServerHelper.getPrefix(), anotherMailServerHelper.getPrefix());
        assertEquals(mailServerHelper.getAddress(), anotherMailServerHelper.getAddress());
        assertEquals(mailServerHelper.getUserName(), anotherMailServerHelper.getUserName());
        assertEquals(mailServerHelper.getJndiLocation(), anotherMailServerHelper.getJndiLocation());


        assertTrue(mailServerHelper.delete());
    }

    public void testUpdateMailServer() throws SAXException
    {
        final MailServerHelper mailServerHelper = getMailServerHelper();

        String mailServerName = "Test Mail Server";
        String mailServerFromAddress = "admin@localhost.localdomain";
        String mailPrefix = "[Test Prefix]";
        String mailServerHostName = "localhost.localdomain";

        createMailServer(mailServerHelper, mailServerName, mailServerFromAddress, mailPrefix, mailServerHostName);

        assertMailServer(mailServerHelper);

        mailServerHelper.setName("Test Mail Server Edited");
        mailServerHelper.setFromAddress("admin@confluence");
        mailServerHelper.setPrefix("[Test Prefix Edited]");
        mailServerHelper.setAddress("localhost:2525");
        mailServerHelper.setUserName("john.doe");

        assertTrue(mailServerHelper.update());

        assertMailServer(mailServerHelper);

        assertTrue(mailServerHelper.delete());
    }

    public void testMailServerIdsAndNames()
    {
        final MailServerHelper mailServerHelper = getMailServerHelper();
        Map mailServerIdsAndNames = mailServerHelper.getMailServerIdMap();

        String mailServerName = "Test Mail Server";
        String mailServerFromAddress = "admin@localhost.localdomain";
        String mailPrefix = "[Test Prefix]";
        String mailServerHostName = "localhost.localdomain";

        assertNotNull(mailServerIdsAndNames);
        assertEquals(0, mailServerIdsAndNames.size());

        createMailServer(mailServerHelper, mailServerName, mailServerFromAddress, mailPrefix, mailServerHostName);

        mailServerIdsAndNames = mailServerHelper.getMailServerIdMap();

        assertNotNull(mailServerIdsAndNames);
        assertEquals(1, mailServerIdsAndNames.size());
        assertEquals(mailServerHelper.getName(), (String) mailServerIdsAndNames.get(String.valueOf(mailServerHelper.getId())));

        assertTrue(mailServerHelper.delete());
    }

    public void testGetMailServerByName()
    {
        final MailServerHelper mailServerHelper = getMailServerHelper();
        List mailServerIds;

        String mailServerName = "Test Mail Server";
        String mailServerFromAddress = "admin@localhost.localdomain";
        String mailPrefix = "[Test Prefix]";
        String mailServerHostName = "localhost.localdomain";

        mailServerHelper.setName(mailServerName);

        mailServerIds = mailServerHelper.getMailServerIdsByName();

        assertNotNull(mailServerIds);
        assertEquals(0, mailServerIds.size());

        createMailServer(mailServerHelper, mailServerName, mailServerFromAddress, mailPrefix, mailServerHostName);

        mailServerIds = mailServerHelper.getMailServerIdsByName();

        assertNotNull(mailServerIds);
        assertEquals(1, mailServerIds.size());
        assertEquals(mailServerHelper.getId(), mailServerIds.get(0));

        assertTrue(mailServerHelper.delete());
    }
}
