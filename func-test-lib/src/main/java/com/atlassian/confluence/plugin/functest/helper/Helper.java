package com.atlassian.confluence.plugin.functest.helper;

import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;

/**
 * The basic methods for all helpers. Since helpers &quot;help&quot; usually
 * create, read and updateMailServer content, this interface defines the methods which
 * do just that.
 */
public interface Helper
{

    ConfluenceWebTester getConfluenceWebTester();

    /**
     * Create the object or concept represented by the implementing {@link Helper}.
     *
     * @return Implementations must <code>true</code> if the creation is successful and
     *         <code>false</code> otherwise.
     */
    boolean create();

    /**
     * Read the object or concept represented by the implementing {@link Helper}.
     *
     * @return Implementations must <code>true</code> if the read is successful and
     *         <code>false</code> otherwise.
     */
    boolean read();

    /**
     * Read the object or concept represented by the implementing {@link Helper}.
     *
     * @return Implementations must <code>true</code> if the updateMailServer is successful and
     *         <code>false</code> otherwise.
     */
    boolean update();

    /**
     * Read the object or concept represented by the implementing {@link Helper}.
     *
     * @return Implementations must <code>true</code> if the deletion is successful and
     *         <code>false</code> otherwise.
     */
    boolean delete();
}
