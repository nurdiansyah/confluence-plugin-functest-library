package com.atlassian.confluence.plugin.functest.module.xmlrpc.content;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.Labelable;
import com.atlassian.confluence.labels.Namespace;
import com.atlassian.confluence.mail.notification.Notification;
import com.atlassian.confluence.mail.notification.NotificationManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.user.User;

public class ContentHelperServiceDelegate implements ContentHelperService
{
    private LabelManager labelManager;

    private NotificationManager notificationManager;

    private PageManager pageManager;

    public void setLabelManager(LabelManager labelManager)
    {
        this.labelManager = labelManager;
    }

    public void setNotificationManager(NotificationManager notificationManager)
    {
        this.notificationManager = notificationManager;
    }

    public void setPageManager(PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

    private ContentEntityObject getContentEntityObject(String entityId)
    {
        long pageId = Long.parseLong(entityId);
        return pageManager.getById(pageId);
    }

    private AbstractPage getPage(String pageIdStr)
    {
        long pageId = Long.parseLong(pageIdStr);
        return pageManager.getAbstractPage(pageId);
    }

    private Notification getNotification(AbstractPage abstractPage)
    {
        return null == abstractPage
                ? null
                : notificationManager.getNotificationByUserAndPage(AuthenticatedUserThreadLocal.getUser(), abstractPage);
    }

    public boolean addWatch(String authenticationToken, String entityId)
    {
        AbstractPage abstractPage = getPage(entityId);

        if (null == abstractPage)
            return false;

        if (null != getNotification(abstractPage))
            return false;

        notificationManager.addPageNotification(
                AuthenticatedUserThreadLocal.getUser(),
                abstractPage
        );

        return true;
    }

    public boolean removeWatch(String authenticationToken, String entityId)
    {
        AbstractPage abstractPage = getPage(entityId);
        Notification notification;

        if (null == abstractPage)
            return false;

        if (null == (notification = getNotification(abstractPage)))
            return false;

        notificationManager.removeNotification(notification);

        return true;
    }

    public boolean isWatched(String authenticationToken, String entityId)
    {
        return null != getNotification(
                getPage(entityId)
        );
    }

    public boolean markFavorite(String authenticationToken, String entityId)
    {
        Labelable contentEntityObject = getContentEntityObject(entityId);
        User currentUser;

        if (null == contentEntityObject)
            return false;

        if (contentEntityObject.isFavourite(currentUser = AuthenticatedUserThreadLocal.getUser()))
            return false;

        labelManager.addLabel(contentEntityObject, new Label(LabelManager.FAVOURITE_LABEL, Namespace.PERSONAL, currentUser.getName()));
        return true;
    }

    public boolean unmarkFavorite(String authentictionToken, String entityId)
    {
        Labelable contentEntityObject = getContentEntityObject(entityId);
        User currentUser;

        if (null == contentEntityObject)
            return false;

        if (!contentEntityObject.isFavourite(currentUser = AuthenticatedUserThreadLocal.getUser()))
            return false;


        String userName = currentUser.getName();

        labelManager.removeLabel(contentEntityObject, new Label(LabelManager.FAVOURITE_LABEL, Namespace.PERSONAL, userName));
        labelManager.removeLabel(contentEntityObject, new Label(LabelManager.FAVOURITE_LABEL_YANKEE, Namespace.PERSONAL, userName));

        return true;
    }

    public boolean isFavorite(String authenticationToken, String entityId)
    {
        ContentEntityObject ceo = getContentEntityObject(entityId);
        return null != ceo && ceo.isFavourite(AuthenticatedUserThreadLocal.getUser());
    }

    public String login(String s, String s1)
    {
        return null;
    }

    public boolean logout(String s)
    {
        return false;
    }
}
