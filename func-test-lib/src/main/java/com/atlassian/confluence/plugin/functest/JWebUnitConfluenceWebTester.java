package com.atlassian.confluence.plugin.functest;

import com.atlassian.confluence.plugin.functest.helper.HelperFactory;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.ConfluenceSoapService;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.ConfluenceSoapServiceServiceLocator;
import com.atlassian.confluence.plugin.functest.util.ClasspathResourceUtil;
import com.atlassian.confluence.plugin.functest.util.ConfluenceBuildUtil;
import com.atlassian.confluence.plugin.functest.util.PluginMetadataUtil;
import junit.framework.Assert;
import net.sourceforge.jwebunit.api.ITestingEngine;
import net.sourceforge.jwebunit.junit.WebTester;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;
import org.w3c.dom.Document;

import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

public class JWebUnitConfluenceWebTester extends WebTester implements ConfluenceWebTester
{
    private static final Logger LOG = Logger.getLogger(JWebUnitConfluenceWebTester.class);

    private static final Set<String> KEYS_OF_PLUGINS_INSTALLED_BY_TEST_LIBRARY = new HashSet<String>();

    private final TesterConfiguration testerConfiguration;

    private String currentUserName;

    private String currentPassword;

    private String licenseString;

    public JWebUnitConfluenceWebTester(TesterConfiguration testerConfiguration)
    {
        this.testerConfiguration = testerConfiguration;

        setCurrentUserName(this.testerConfiguration.getAdminUserName());
        setCurrentPassword(this.testerConfiguration.getAdminPassword());
        getTestContext().setBaseUrl(this.testerConfiguration.getBaseUrl());
        setScriptingEnabled(false);
    }

    public String getBaseUrl()
    {
        return testerConfiguration.getBaseUrl();
    }

    public String getAdminUserName()
    {
        return testerConfiguration.getAdminUserName();
    }

    public String getAdminPassword()
    {
        return testerConfiguration.getAdminPassword();
    }

    public String getCurrentUserName()
    {
        return currentUserName;
    }

    public void setCurrentUserName(String currentUserName)
    {
        this.currentUserName = currentUserName;
    }

    public String getCurrentPassword()
    {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword)
    {
        this.currentPassword = currentPassword;
    }

    public void login(String username, String password)
    {
        setCurrentUserName(username);
        setCurrentPassword(password);
        login();
    }

    public void login()
    {
        gotoPage("/login.action");
        assertTextNotPresent("You are currently logged in");

        setWorkingForm("loginform");
        setTextField("os_username", getCurrentUserName());
        setTextField("os_password", getCurrentPassword());
        submit("login");

        assertLinkPresentWithText("Log Out");
    }

    public void logout()
    {
        gotoPage("/logout.action");
        assertTextPresent("You have been successfully logged out");
    }

    public XmlRpcClient getXmlRpcClient() throws MalformedURLException
    {
        return new XmlRpcClient(new StringBuilder(getBaseUrl()).append("/rpc/xmlrpc").toString());
    }

    public ConfluenceSoapService getConfluenceSoapService() throws MalformedURLException, ServiceException
    {
        ConfluenceSoapServiceServiceLocator confluenceSoapServiceServiceLocator = new ConfluenceSoapServiceServiceLocator();
        return confluenceSoapServiceServiceLocator.getConfluenceserviceV1(
                new URL(new StringBuilder(getBaseUrl()).append("/rpc/soap-axis/confluenceservice-v1?wsdl").toString())
        );
    }

    public String loginToXmlRpcService(final String userName, final String password) throws XmlRpcException, IOException
    {
        final XmlRpcClient xmlRpcClient = getXmlRpcClient();

        return (String) xmlRpcClient.execute("confluence1.login",
                new Vector<String>(
                        Arrays.asList(userName, password)
                ));
    }

    public void logoutFromXmlRpcService(final String authenticationToken)
    {
        final XmlRpcClient xmlRpcClient;

        if (StringUtils.isNotBlank(authenticationToken))
        {
            try
            {
                xmlRpcClient = getXmlRpcClient();
                xmlRpcClient.execute("confluence1.logout",
                        new Vector<String>(
                                Arrays.asList(
                                        authenticationToken
                                )
                        ));
            }
            catch (final MalformedURLException mUrlE)
            {
                LOG.error("Invalid RPC URL specified.", mUrlE);
            }
            catch (final XmlRpcException xmlRpcE)
            {
                LOG.error("Service request denied.", xmlRpcE);
            }
            catch (final RemoteException re)
            {
                LOG.error("There's an error in Confluence.", re);
            }
            catch (final IOException ioe)
            {
                LOG.error("Can't talk to Confluence.", ioe);
            }
        }
    }

    public String loginToXmlRPcService() throws XmlRpcException, IOException
    {
        return loginToXmlRpcService(getCurrentUserName(), getCurrentPassword());
    }

    public String loginToSoapService(final String userName, final String password)
            throws MalformedURLException, ServiceException, RemoteException
    {
        final ConfluenceSoapService confluenceSoapService = getConfluenceSoapService();

        return StringUtils.isNotBlank(userName) ? confluenceSoapService.login(userName, password) : null;
    }

    public String loginToSoapService() throws MalformedURLException, ServiceException, RemoteException
    {
        return loginToSoapService(getCurrentUserName(), getCurrentPassword());
    }

    public void logoutFromSoapService(final String authenticationToken)
    {
        try
        {
            if (null != authenticationToken)
            {
                getConfluenceSoapService().logout(authenticationToken);
            }

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
    }

    public void updateLicense() throws IOException
    {
        final String licenseString = getLicenseString();

        if (StringUtils.isNotBlank(licenseString))
        {
            try
            {
                Assert.assertTrue(gotoPageWithEscalatedPrivileges("/admin/console.action"));
                clickLinkWithText("License Details");

                setWorkingForm("updateLicenseForm");
                setTextField("licenseString", licenseString);

                submit("update");
                assertTextNotPresent("License string is too short");
                assertTextNotPresent("License was not valid");
            }
            finally
            {
                dropEscalatedPrivileges();
            }
        }
    }

    public void refreshLicense()
    {
        try { Assert.assertTrue(gotoPageWithEscalatedPrivileges("/admin/refreshlicensing.action")); }
        finally { dropEscalatedPrivileges(); }
    }

    public void installTestLibrary()
    {
        File testLibraryJar = testerConfiguration.getTestLibraryJar();

        if (null != testLibraryJar && testLibraryJar.isFile())
        {
            try
            {
                Assert.assertTrue(gotoPageWithEscalatedPrivileges("/admin/console.action"));

                String pluginKey = PluginMetadataUtil.getPluginKey(
                        PluginMetadataUtil.getPluginDescriptorDom(testLibraryJar)
                );

                if (!isPluginInstalled(pluginKey))
                {
                    LOG.info("Installing test library with key " + pluginKey);
                    installPlugin(testLibraryJar);
                }
                else
                {
                    LOG.info("Plugin test library already installed. Skipping installation.");
                }

            }
            catch (Exception e)
            {
                Assert.fail("Unable to determine plugin key from " + testLibraryJar + SystemUtils.LINE_SEPARATOR +
                        ExceptionUtils.getFullStackTrace(e));
            }
            finally
            {
                dropEscalatedPrivileges();
            }
        }
    }

    public void installPlugin()
    {
        File pluginJar = testerConfiguration.getPluginJar();
        if (testerConfiguration.isInstallPlugin() && null != pluginJar)
            installPlugin(pluginJar);
    }

    public void installPlugin(File pluginJar)
    {
        if (null != pluginJar && pluginJar.isFile())
        {
            try
            {
                Document pluginDescDom = PluginMetadataUtil.getPluginDescriptorDom(pluginJar);
                String pluginKey = PluginMetadataUtil.getPluginKey(pluginDescDom);
                String pluginName = PluginMetadataUtil.getPluginName(pluginDescDom);

                if (!hasTestLibraryInstalledPlugin(pluginKey))
                {
                    LOG.info("Installing plugin \"" + pluginName + "\" with key " + pluginKey);
                    installPluginInternal(pluginJar);
                    markPluginInstalledByTestLibrary(pluginKey);
                }
                else
                {
                    LOG.info("Plugin \"" + pluginName + "\" already installed. Skipping installation.");
                }
            }
            catch (Exception e)
            {
                Assert.fail("Unable to determine plugin key from " + pluginJar + SystemUtils.LINE_SEPARATOR + ExceptionUtils.getFullStackTrace(e));
            }
        }
        else
        {
            LOG.warn("Invalid plugin JAR (" + pluginJar + ") specified for install. Skipping");
        }
    }

    private boolean isPluginInstalled(String pluginKey)
    {
        if (isConfluence210AndBelow())
            gotoPage("/admin/plugins.action?pluginKey=" + pluginKey);
        else
            gotoPage("/admin/viewplugins.action?pluginKey="+pluginKey);
        ITestingEngine webDialog = getTestingEngine();

        return webDialog.hasElementByXPath("//a[text()='Enable plugin']") || webDialog.hasElementByXPath("//a[text()='Disable plugin']");
    }
	
    private boolean isConfluence210AndBelow()
    {
        return ConfluenceBuildUtil.getBuildNumber() < 1600;
    }

    private boolean hasTestLibraryInstalledPlugin(String pluginKey)
    {
        return KEYS_OF_PLUGINS_INSTALLED_BY_TEST_LIBRARY.contains(pluginKey);
    }

    private void markPluginInstalledByTestLibrary(String pluginKey)
    {
        KEYS_OF_PLUGINS_INSTALLED_BY_TEST_LIBRARY.add(pluginKey);
    }

    public void installPluginInternal(File pluginJar)
    {
        try
        {
            Assert.assertTrue(gotoPageWithEscalatedPrivileges("/admin/viewplugins.action"));
            assertTextPresent("Manage Plugins");

            setWorkingForm("plugin-upload");

            setTextField("file_0", pluginJar.getAbsolutePath());
            submit("confirm");

            String pluginName = PluginMetadataUtil.getPluginName(PluginMetadataUtil.getPluginDescriptorDom(pluginJar));
            assertLinkPresentWithText(pluginName);
        }
        catch (Exception e)
        {
            Assert.fail("Unable to get plugin name from plugin JAR at " + pluginJar + "\n" + ExceptionUtils.getFullStackTrace(e));
        }
        finally
        {
            dropEscalatedPrivileges();
        }
    }

    /**
     * Restore the class path resource &quot;site-export.zip&quot; into Confluence.
     * @deprecated
     * Please use {@link #restoreData(java.io.File)} instead.
     */
    public void restoreData()
    {
        File configuredSiteExport = testerConfiguration.getSiteBackupZip();
        File siteExportZip  = null;

        try
        {
            if (null != configuredSiteExport)
            {
                // Restore the configured site backup
                restoreData(configuredSiteExport);
            }
            else
            {
                // Try to restore a file named site-export.zip for backwards compatibility.
                // The 'legacy' parent POM sets ${basedir}src/test/resources/site-export.zip as the configured site backup to restore.
                siteExportZip = ClasspathResourceUtil.getClassPathResourceAsTempFile("site-export.zip", getClass().getClassLoader(), ".zip");
                if (null != siteExportZip)
                    restoreData(siteExportZip);
            }
        }
        catch (IOException ioe)
        {
            Assert.fail("Unable to restore class path site-export.zip: " + ioe.getMessage());
        }
        finally
        {
            if (null != siteExportZip && siteExportZip.exists())
                Assert.assertTrue("Unable to delete " + siteExportZip, siteExportZip.delete());
        }
    }

    public void restoreData(File exportZip)
    {
        if (null != exportZip && exportZip.isFile())
        {
            try
            {
                String destination = "/admin/backup.action?synchronous=true";

                gotoPageWithEscalatedPrivileges(destination);
                assertTextPresent("Restore Confluence Data");

                setWorkingForm(2);
                setTextField("file", exportZip.getAbsolutePath());
                submit();

                // Update base URL
                gotoPage("/admin/editgeneralconfig.action");
                setWorkingForm("editgeneralconfig");
                setTextField("domainName", getBaseUrl());
                submit("confirm");

                HelperFactory.createIndexHelper(this).update();
            }
            finally
            {
                dropEscalatedPrivileges();
            }
        }
        else
        {
            LOG.warn("Unable to find export file: " + exportZip + ". Skipping data restoration.");
        }
    }

    public void flushCaches()
    {
        try
        {
            Assert.assertTrue(gotoPageWithEscalatedPrivileges("/admin/console.action"));
            clickLinkWithText("Cache Statistics");
            clickLinkWithText("Flush all");
        }
        finally
        {
            dropEscalatedPrivileges();
        }
    }

    public String getLicenseString()
    {
        return StringUtils.defaultIfEmpty(licenseString, testerConfiguration.getLicense());
    }

    public void setLicenseString(String licenseString)
    {
        this.licenseString = licenseString;
    }

    private String assertXsrfStatusCode(String resourcePath)
    {
        StringBuilder urlWithOsAuthTypeReplacedBuilder = new StringBuilder(getBaseUrl())
                .append(
                        resourcePath.replaceAll(
                                "(.*[?&])(os_authType=\\w*)(.*)",
                                "$1os_authType=basic$3"
                        )
                );

        if (urlWithOsAuthTypeReplacedBuilder.indexOf("os_authType") < 0)
        {
            urlWithOsAuthTypeReplacedBuilder.append(urlWithOsAuthTypeReplacedBuilder.indexOf("?") < 0 ? '?' : '&')
                    .append("os_authType=basic");
        }

        HttpClient client = new HttpClient();
        GetMethod httpGet = new GetMethod(urlWithOsAuthTypeReplacedBuilder.toString());

        try
        {
            client.getState().setCredentials(
                    new AuthScope(
                            AuthScope.ANY_HOST,
                            AuthScope.ANY_PORT
                    ),
                    new UsernamePasswordCredentials(
                            getCurrentUserName(),
                            getCurrentPassword()
                    )
            );
            httpGet.setDoAuthentication(true);

            Assert.assertEquals(
                    HttpServletResponse.SC_FORBIDDEN,
                    client.executeMethod(httpGet)
            );

            return httpGet.getResponseBodyAsString();
        }
        catch (IOException ioe)
        {
            LOG.error("Error getting HTTP resource " + urlWithOsAuthTypeReplacedBuilder, ioe);
            return null;
        }
        finally
        {
            httpGet.releaseConnection();
        }
    }

    /**
     * Asserts that a resource is XSRF protected.
     *
     * @param resourcePath The resource to test for XSRF protection. It must not contain the XSRF token. Must be absolute (e.g. <tt>/admin/plugins.action</tt>).
     */
    public void assertResourceXsrfProtected(String resourcePath)
    {
        Assert.assertFalse("No body text returned for " + resourcePath, StringUtils.isBlank(assertXsrfStatusCode(resourcePath)));
    }

    public void assertXsrfTokenNotPresentFailure(String resourcePath)
    {
        Assert.assertTrue(
                assertXsrfStatusCode(resourcePath).indexOf("a required security token was not present") >= 0
        );
    }

    public void assertXsrfTokenNotValidFailure(String resourcePath)
    {
        Assert.assertTrue(
                assertXsrfStatusCode(resourcePath).indexOf("Your session has expired. You may need to re-submit the form or reload the page.") >= 0
        );
    }

    public boolean gotoPageWithEscalatedPrivileges(String destination)
    {
        return gotoPageWithEscalatedPrivileges(destination, "UTF-8");
    }

    public boolean gotoPageWithEscalatedPrivileges(String destination, String urlEncoding)
    {
        if (ConfluenceBuildUtil.containsSudoFeature() && !getDialog().hasElementByXPath("//*[@id='confluence-message-websudo-message']"))
        {
            try
            {
                gotoPage("/authenticate.action?destination=" + URLEncoder.encode(destination, urlEncoding));
                setWorkingForm("authenticateform");
                setTextField("password", getCurrentPassword());
                submit("authenticate");
            }
            catch (UnsupportedEncodingException uee)
            {
                LOG.error("Unable to URL encode " + destination + " with " + urlEncoding, uee);
                return false;
            }
        }
        else
        {
            gotoPage(destination);
        }

        return true;
    }

    public void dropEscalatedPrivileges()
    {
        if (ConfluenceBuildUtil.containsSudoFeature() && getDialog().hasElementByXPath("//*[@id='confluence-message-websudo-message']"))
            gotoPage("/dropauthentication.action");
    }
}
