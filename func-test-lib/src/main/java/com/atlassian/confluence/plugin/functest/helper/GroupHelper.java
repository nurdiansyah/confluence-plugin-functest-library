package com.atlassian.confluence.plugin.functest.helper;

import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.ConfluenceSoapService;
import org.apache.log4j.Logger;

import javax.xml.rpc.ServiceException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;

public class GroupHelper extends AbstractHelper
{

    private static final Logger LOG = Logger.getLogger(GroupHelper.class);

    private String name;

    public GroupHelper(final ConfluenceWebTester confluenceWebTester, final String name)
    {
        super(confluenceWebTester);
        setName(name);
    }

    public GroupHelper(final ConfluenceWebTester confluenceWebTester)
    {
        this(confluenceWebTester, null);
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public boolean create()
    {
        String soapSessionToken = null;
        ConfluenceSoapService confluenceSoapService;

        try
        {
            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            confluenceSoapService.addGroup(soapSessionToken, getName());
            return true;

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return false;
    }

    public boolean read()
    {
        String soapSessionToken = null;
        ConfluenceSoapService confluenceSoapService;

        try
        {
            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            return confluenceSoapService.hasGroup(soapSessionToken, getName());

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return false;
    }

    public boolean update()
    {
        throw new UnsupportedOperationException("Group edits currently not supported.");
    }

    public boolean delete()
    {
        String soapSessionToken = null;
        ConfluenceSoapService confluenceSoapService;

        try
        {
            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            return confluenceSoapService.removeGroup(soapSessionToken, getName(), null);

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return false;
    }
}
