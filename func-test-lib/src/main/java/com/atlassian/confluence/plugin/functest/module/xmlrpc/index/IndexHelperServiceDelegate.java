package com.atlassian.confluence.plugin.functest.module.xmlrpc.index;

import com.atlassian.bonnie.search.BatchIndexer;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.search.lucene.ConfluenceIndexManager;
import com.atlassian.core.util.ProgressMeter;
import com.atlassian.spring.container.ComponentNotFoundException;
import com.atlassian.spring.container.ContainerManager;
import org.apache.log4j.Logger;

import java.io.IOException;


public class IndexHelperServiceDelegate implements IndexHelperService
{
    private static final Logger logger = Logger.getLogger(IndexHelperServiceDelegate.class);

    public Boolean reindex(final String authenticationToken)
    {
        try
        {
            getIndexer().indexEntities(new ProgressMeter());

            return Boolean.TRUE;
        }
        catch (IOException ioe)
        {
            logger.error("Unable to reindex Confluence.", ioe);
            return Boolean.FALSE;
        }
    }

    public Boolean flush(final String authenticationToken) {
        try
        {
            getIndexManager().flushQueue();

            return Boolean.TRUE;
        }
        catch (IOException ioe)
        {
            logger.error("Unable to flush index queue.", ioe);
            return Boolean.FALSE;
        }
    }

    private BatchIndexer getIndexer() throws IOException
    {
        try
        {
            return (BatchIndexer) ContainerManager.getComponent("indexRebuilder");
        }
        catch (ComponentNotFoundException cnfe)
        {
            return (BatchIndexer) ContainerManager.getComponent("confluenceBatchIndexer");
        }
    }

    private ConfluenceIndexManager getIndexManager() throws IOException
    {
        return (ConfluenceIndexManager) ContainerManager.getComponent("indexManager");
    }

    public String login(String s, String s1) throws RemoteException
    {
        return null;
    }

    public boolean logout(String s) throws RemoteException
    {
        return false;
    }
}
