package com.atlassian.confluence.plugin.functest.module.xmlrpc.comment;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.CommentManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.user.User;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class CommentHelperServiceDelegate implements CommentHelperService
{
    private static final Logger LOG = LoggerFactory.getLogger(CommentHelperServiceDelegate.class);

    private PageManager pageManager;

    private CommentManager commentManager;

    private PermissionManager permissionManager;

    private XhtmlContent xhtmlContent;

    @SuppressWarnings("unused")
    public void setPageManager(PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

    @SuppressWarnings("unused")
    public void setCommentManager(CommentManager commentManager)
    {
        this.commentManager = commentManager;
    }

    @SuppressWarnings("unused")
    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    @SuppressWarnings("unused")
    public void setXhtmlContent(XhtmlContent xhtmlContent)
    {
        this.xhtmlContent = xhtmlContent;
    }

    @Override
    public Map<String, ?> getComment(String authenticationToken, String commentId)
    {
        Comment comment = commentManager.getComment(Long.parseLong(commentId));
        Map<String, Object> commentStruct = null;

        if (null != comment && permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, comment))
        {
            commentStruct = new Hashtable<String, Object>();

            commentStruct.put("id", comment.getIdAsString());

            if (null != comment.getParent())
                commentStruct.put("parentId", comment.getParent().getIdAsString());

            commentStruct.put("ownerId", comment.getOwner().getIdAsString());
            commentStruct.put("content", comment.getBodyAsString());

            if (StringUtils.isNotBlank(comment.getCreatorName()))
                commentStruct.put("creator", comment.getCreatorName());

            commentStruct.put("created", comment.getCreationDate());

            if (StringUtils.isNotBlank(comment.getLastModifierName()))
                commentStruct.put("lastModifier", comment.getLastModifierName());

            commentStruct.put("lastModified", comment.getLastModificationDate());
        }

        return commentStruct;
    }

    @Override
    public List<String> getCommentIds(String authenticationToken, String pageId)
    {
        AbstractPage page = pageManager.getAbstractPage(Long.parseLong(pageId));
        List<String> commentIds = new Vector<String>();

        if (null != page && permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(),  Permission.VIEW, page))
        {
            commentIds.addAll(
                    Collections2.transform(
                            page.getComments(),
                            new Function<Comment, String>()
                            {
                                @Override
                                public String apply(Comment comment)
                                {
                                    return comment.getIdAsString();
                                }
                            }
                    )
            );
        }

        return commentIds;
    }

    @Override
    public String addOrUpdateComment(String authenticationToken, Hashtable<String, ?> commentStruct)
    {
        String pageIdStr = (String) commentStruct.get("pageId");

        if (StringUtils.isNotBlank(pageIdStr) && StringUtils.isNumeric(pageIdStr))
        {
            AbstractPage commentPage = pageManager.getAbstractPage(Long.parseLong(pageIdStr));
            if (null !=  commentPage)
            {
                User user = AuthenticatedUserThreadLocal.getUser();
                String commentIdStr = (String) commentStruct.get("id");


                if (StringUtils.isNotBlank(commentIdStr) && StringUtils.isNumeric(commentIdStr))
                {
                    Comment pageComment = commentManager.getComment(Long.parseLong(commentIdStr));
                    if (null != pageComment)
                    {
                        List<RuntimeException> conversionErrors = new ArrayList<RuntimeException>();
                        commentManager.updateCommentContent(
                                pageComment,
                                xhtmlContent.convertWikiToStorage(
                                        StringUtils.defaultString((String) commentStruct.get("content")),
                                        new DefaultConversionContext(commentPage.toPageContext()),
                                        conversionErrors
                                )
                        );

                        if (conversionErrors.isEmpty())
                        {
                            return pageComment.getIdAsString();
                        }
                        else
                        {
                            for (RuntimeException conversionError : conversionErrors)
                                LOG.error("Error converting wiki markup to storage.", conversionError);
                        }
                    }
                }
                else if (permissionManager.hasCreatePermission(user, commentPage, Comment.class))
                {
                    String parentCommentIdStr = (String) commentStruct.get("parentId");
                    Comment parentComment = null;

                    if (StringUtils.isNotBlank(parentCommentIdStr) && StringUtils.isNumeric(parentCommentIdStr))
                        parentComment = commentManager.getComment(Long.parseLong(parentCommentIdStr));

                    List<RuntimeException> conversionErrors = new ArrayList<RuntimeException>();

                    try
                    {
                        return commentManager.addCommentToObject(
                                commentPage,
                                parentComment,
                                xhtmlContent.convertWikiToStorage(
                                        StringUtils.defaultString((String) commentStruct.get("content")),
                                        new DefaultConversionContext(commentPage.toPageContext()),
                                        conversionErrors
                                )
                        ).getIdAsString();
                    }
                    finally
                    {
                        for (RuntimeException conversionError : conversionErrors)
                            LOG.error("Error converting wiki markup to storage.", conversionError);
                    }
                }
            }
        }

        return "";
    }

    @Override
    public String login(String s, String s1) throws RemoteException
    {
        return null;
    }

    @Override
    public boolean logout(String s) throws RemoteException
    {
        return false;
    }
}
