package com.atlassian.confluence.plugin.functest.helper;

import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.ConfluenceSoapService;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.RemoteBlogEntrySummary;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.RemotePageSummary;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.RemoteSpace;
import com.atlassian.confluence.spaces.SpaceType;
import org.apache.log4j.Logger;

import javax.xml.rpc.ServiceException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

public class SpaceHelper extends AbstractHelper
{

    private static final Logger LOG = Logger.getLogger(SpaceHelper.class);

    public static final String TYPE_GLOBAL = SpaceType.GLOBAL.toString();

    public static final String TYPE_PERSONAL = SpaceType.PERSONAL.toString();

    private String key;

    private String type;

    private String name;

    private String description;

    private long homePageId;

    public SpaceHelper(final ConfluenceWebTester confluenceWebTester)
    {
        this(confluenceWebTester, null);
    }

    public SpaceHelper(final ConfluenceWebTester confluenceWebTester, final String spaceKey)
    {
        super(confluenceWebTester);
        setKey(spaceKey);
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public long getHomePageId()
    {
        return homePageId;
    }

    public void setHomePageId(long homePageId)
    {
        this.homePageId = homePageId;
    }

    public Set getPageIds()
    {
        final Set<Long> pageIdSet = new HashSet<Long>();
        String soapSessionToken = null;

        try
        {
            final ConfluenceSoapService confluenceSoapService;
            final RemotePageSummary[] remotePageSummaries;

            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            remotePageSummaries = confluenceSoapService.getPages(soapSessionToken, getKey());
            if (null != remotePageSummaries)
            {
                for (RemotePageSummary remotePageSummary : remotePageSummaries)
                {
                    pageIdSet.add(remotePageSummary.getId());
                }
            }

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return pageIdSet;
    }

    public Set getBlogIds()
    {
        final Set<Long> blogIdSet = new HashSet<Long>();
        String soapSessionToken = null;

        try
        {
            final ConfluenceSoapService confluenceSoapService;
            final RemoteBlogEntrySummary[] remoteBlogEntrySummaries;

            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            remoteBlogEntrySummaries = confluenceSoapService.getBlogEntries(soapSessionToken, getKey());
            if (null != remoteBlogEntrySummaries)
            {
                for (RemoteBlogEntrySummary remoteBlogEntrySummary : remoteBlogEntrySummaries)
                {
                    blogIdSet.add(remoteBlogEntrySummary.getId());
                }
            }

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return blogIdSet;
    }

    protected RemoteSpace toRemoteSpace()
    {
        RemoteSpace remoteSpace = new RemoteSpace();

        remoteSpace.setKey(getKey());
        remoteSpace.setType(getType());
        remoteSpace.setName(getName());
        remoteSpace.setDescription(getDescription());
        remoteSpace.setHomePage(getHomePageId());

        return remoteSpace;
    }

    public boolean create()
    {
        String soapSessionToken = null;

        try
        {
            final ConfluenceSoapService confluenceSoapService;

            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            confluenceSoapService.addSpace(soapSessionToken, toRemoteSpace());

            return true;

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return false;
    }

    public boolean read()
    {
        String soapSessionToken = null;

        try
        {
            final ConfluenceSoapService confluenceSoapService;
            final RemoteSpace remoteSpace;

            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            remoteSpace = confluenceSoapService.getSpace(soapSessionToken, getKey());

            setKey(remoteSpace.getKey());
            setType(remoteSpace.getType());
            setName(remoteSpace.getName());
            setDescription(remoteSpace.getDescription());
            setHomePageId(remoteSpace.getHomePage());

            return true;

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return false;
    }

    public boolean update()
    {
//        String soapSessionToken = null;
//
//        try {
//            final ConfluenceSoapService confluenceSoapService;
//
//            soapSessionToken = confluenceWebTester.loginToSoapService();
//            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();
//
//            confluenceSoapService.storeSpace(soapSessionToken, toRemoteSpace());
//
//            return true;
//
//        } catch (final MalformedURLException mUrlE) {
//            handleInvalidSoapServiceEndpointException(mUrlE);
//        } catch (final ServiceException se) {
//            handleInvalidSoapServiceException(se);
//        } catch (final RemoteException re) {
//            handleRemoteException(re);
//        } finally {
//            confluenceWebTester.logoutFromSoapService(soapSessionToken);
//        }
//
//        return false;
        throw new UnsupportedOperationException("The operation would not be supported until com.atlassian.confluence.rpc.soap.ConfluenceSoapService#storeSpace stops calling addSpace");
    }

    public boolean delete()
    {
        String soapSessionToken = null;

        try
        {
            final ConfluenceSoapService confluenceSoapService;

            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            return confluenceSoapService.removeSpace(soapSessionToken, getKey());

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return false;
    }
}
