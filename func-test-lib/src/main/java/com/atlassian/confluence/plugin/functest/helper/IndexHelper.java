package com.atlassian.confluence.plugin.functest.helper;

import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.Vector;

public class IndexHelper extends AbstractHelper
{
    private static final Logger LOG = Logger.getLogger(IndexHelper.class);

    public IndexHelper(ConfluenceWebTester confluenceWebTester)
    {
        super(confluenceWebTester);
    }

    /**
     * Throws {@link UnsupportedOperationException} immediately. Index creation is not supported. You can only
     * reindex (update).
     *
     * @return Irrelevant due to the exception thrown.
     */
    public boolean create()
    {
        throw new UnsupportedOperationException("Index creation not supported.");
    }

    /**
     * Throws {@link UnsupportedOperationException} immediately. Index read is not supported. You can only
     * reindex (update).
     *
     * @return Irrelevant due to the exception thrown.
     */
    public boolean read()
    {
        throw new UnsupportedOperationException("Index read not supported.");
    }

    public boolean update()
    {
        return exec("reindex");
    }

    public boolean flush()
    {
        return exec("flush");
    }

    private boolean exec(final String method) {
        String authenticationToken = null;
        try
        {
            final XmlRpcClient xmlRpcClient = confluenceWebTester.getXmlRpcClient();
            authenticationToken = confluenceWebTester.loginToXmlRPcService();
            return (Boolean) xmlRpcClient.execute("functest-index."+ method, new Vector<String>(Arrays.asList(authenticationToken)));
        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final XmlRpcException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
        }
        return false;
    }

    /**
     * Throws {@link UnsupportedOperationException} immediately. Index deletion is not supported. You can only
     * reindex (update).
     *
     * @return Irrelevant due to the exception thrown.
     */
    public boolean delete()
    {
        throw new UnsupportedOperationException("Index deletion not supported.");
    }
}
