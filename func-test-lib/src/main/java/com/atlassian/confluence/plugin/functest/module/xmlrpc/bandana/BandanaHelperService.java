package com.atlassian.confluence.plugin.functest.module.xmlrpc.bandana;

import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.rpc.SecureRpc;

import java.util.Map;

public interface BandanaHelperService extends SecureRpc
{
    Boolean setValue(final String authenticationToken, final String spaceKey, final String key, final String value) throws RemoteException;

    Boolean removeValue(final String authenticationToken, final String spaceKey, final String key) throws RemoteException;

    Map<String, Object> getValue(final String authenticationToken, final String spaceKey, final String key) throws RemoteException;
}
