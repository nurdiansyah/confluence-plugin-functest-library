package com.atlassian.confluence.plugin.functest.helper;

import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.ConfluenceSoapService;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.RemoteAttachment;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.RemotePage;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.RemotePageSummary;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.xml.rpc.ServiceException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class PageHelper extends AbstractSpaceContentHelper implements Attachable
{

    private static final Logger LOG = Logger.getLogger(PageHelper.class);

    private long parentId;

    private String lastModifier;

    private Date lastModifiedDate;

    public PageHelper(final ConfluenceWebTester confluenceWebTester, final long id)
    {
        super(confluenceWebTester, id);
    }

    public PageHelper(final ConfluenceWebTester confluenceWebTester)
    {
        this(confluenceWebTester, 0);
    }

    public long getParentId()
    {
        return parentId;
    }

    public void setParentId(long parentId)
    {
        this.parentId = parentId;
    }

    public String getLastModifier()
    {
        return lastModifier;
    }

    public void setLastModifier(String lastModifier)
    {
        this.lastModifier = lastModifier;
    }

    public Date getLastModifiedDate()
    {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate)
    {
        this.lastModifiedDate = lastModifiedDate;
    }

    public List getChildrenPageIds()
    {
        final RemotePageSummary[] remotePageSummaries;
        String soapSessionToken = null;
        ConfluenceSoapService confluenceSoapService;


        if (getId() > 0)
        {
            try
            {
                final List<Long> pageIds;

                soapSessionToken = confluenceWebTester.loginToSoapService();
                confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

                remotePageSummaries = confluenceSoapService.getChildren(soapSessionToken, getId());
                if (null != remotePageSummaries)
                {
                    pageIds = new ArrayList<Long>(remotePageSummaries.length);

                    for (RemotePageSummary remotePageSummary : remotePageSummaries)
                    {
                        pageIds.add(remotePageSummary.getId());
                    }

                    return pageIds;
                }

            }
            catch (final MalformedURLException mUrlE)
            {
                LOG.error("Invalid RPC URL specified.", mUrlE);
            }
            catch (final ServiceException se)
            {
                LOG.error("Service request denied.", se);
            }
            catch (final RemoteException re)
            {
                LOG.error("There's an error in Confluence.", re);
            }
            catch (final IOException ioe)
            {
                LOG.error("Can't talk to Confluence.", ioe);
            }
            finally
            {
                confluenceWebTester.logoutFromSoapService(soapSessionToken);
            }
        }

        return null;
    }

    protected RemotePage toRemotePage()
    {
        final RemotePage remotePage = new RemotePage();

        remotePage.setId(getId());
        remotePage.setSpace(getSpaceKey());
        remotePage.setParentId(getParentId());
        remotePage.setTitle(getTitle());
        remotePage.setVersion(getVersion());
        remotePage.setContent(getContent());
        remotePage.setCreator(StringUtils.isBlank(getCreator()) ? confluenceWebTester.getCurrentUserName() : getCreator());

        if (null != getCreationDate())
        {
            final Calendar creationDate = Calendar.getInstance();

            creationDate.setTime(getCreationDate());
            remotePage.setCreated(creationDate);
        }

        remotePage.setModifier(StringUtils.isBlank(getLastModifier()) ? confluenceWebTester.getCurrentUserName() : getLastModifier());

        if (null != getLastModifiedDate())
        {
            final Calendar lastModifiedDate = Calendar.getInstance();

            lastModifiedDate.setTime(getLastModifiedDate());
            remotePage.setModified(lastModifiedDate);
        }

        return remotePage;
    }

    protected void populateHelper(final Map<String, ?> pageStruct)
    {
        setId(Long.parseLong((String) pageStruct.get("id")));
        setSpaceKey((String) pageStruct.get("spaceKey"));
        setParentId(pageStruct.containsKey("parentId") ? Long.parseLong((String) pageStruct.get("parentId")) : 0L);
        setTitle((String) pageStruct.get("title"));
        setVersion((Integer) pageStruct.get("version"));
        setContent((String) pageStruct.get("content"));
        setCreator((String) pageStruct.get("creator"));
        setCreationDate((Date) pageStruct.get("created"));
        setLastModifier((String) pageStruct.get("lastModifier"));
        setLastModifiedDate((Date) pageStruct.get("lastModified"));
    }

    protected boolean storeRemoteContentAndUpdateHelper(
            final String soapSessionToken,
            final ConfluenceSoapService confluenceSoapService) throws MalformedURLException, ServiceException, RemoteException
    {
        final RemotePage remotePage = confluenceSoapService.storePage(soapSessionToken, toRemotePage());

        setId(remotePage.getId());
        setVersion(remotePage.getVersion());
        return 0 < getId();
    }

    protected boolean readRemoteContentAndUpdateHelper(
            final String soapSessionToken,
            final ConfluenceSoapService confluenceSoapService) throws MalformedURLException, ServiceException, RemoteException
    {
        boolean success = false;
        String authenticationToken = null;

        try
        {
            authenticationToken = confluenceWebTester.loginToXmlRPcService();

            @SuppressWarnings("unchecked")
            Map<String, ?> pageStruct = (Map<String, ?>) confluenceWebTester.getXmlRpcClient().execute("functest-page.getPage",
                    new Vector<Object>(
                            Arrays.asList(
                                    authenticationToken,
                                    String.valueOf(getId())
                            )
                    ));
            if ((success = null != pageStruct))
                populateHelper(pageStruct);
        }
        catch (Exception e)
        {
            LOG.error(String.format("Unable to find blog post with ID %d", getId()), e);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
        }

        return success;
    }

    protected boolean deleteRemoteContent(
            final String soapSessionToken,
            final ConfluenceSoapService confluenceSoapService) throws MalformedURLException, ServiceException, RemoteException
    {
        return confluenceSoapService.removePage(soapSessionToken, getId());
    }

    public String[] getAttachmentFileNames()
    {
        String soapSessionToken = null;

        try
        {
            final ConfluenceSoapService confluenceSoapService;
            final RemoteAttachment[] remoteAttachments;
            final String[] attachmentNames;

            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            remoteAttachments = confluenceSoapService.getAttachments(soapSessionToken, getId());

            if (null != remoteAttachments)
            {
                attachmentNames = new String[remoteAttachments.length];

                for (int i = 0; i < remoteAttachments.length; ++i)
                {
                    attachmentNames[i] = remoteAttachments[i].getFileName();
                }

            }
            else
            {
                attachmentNames = new String[0];
            }

            return attachmentNames;

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return null;
    }

    public long findBySpaceKeyAndTitle()
    {
        String soapSessionToken = null;

        try
        {
            final ConfluenceSoapService confluenceSoapService;
            final RemotePageSummary remotePageSummary;

            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            remotePageSummary = confluenceSoapService.getPageSummary(soapSessionToken, getSpaceKey(), getTitle());

            if (null != remotePageSummary)
            {
                return remotePageSummary.getId();
            }

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return 0;
    }
}
