package com.atlassian.confluence.plugin.functest;

import com.atlassian.confluence.plugin.functest.remote.soap.stub.ConfluenceSoapService;
import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;

import javax.xml.rpc.ServiceException;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;


public interface ConfluenceWebTester
{
    String getAdminUserName();

    String getAdminPassword();

    String getCurrentUserName();

    void setCurrentUserName(String currentUserName);

    String getCurrentPassword();

    void setCurrentPassword(String currentPassword);

    String getLicenseString();

    void setLicenseString(String licenseString);

    String getBaseUrl();

    void login(final String username, final String password);

    void login();

    void logout();

    XmlRpcClient getXmlRpcClient() throws MalformedURLException;

    String loginToXmlRpcService(final String userName, final String password) throws XmlRpcException, IOException;

    void logoutFromXmlRpcService(final String authenticationToken);

    String loginToXmlRPcService() throws XmlRpcException, IOException;

    ConfluenceSoapService getConfluenceSoapService() throws MalformedURLException, ServiceException;

    String loginToSoapService(final String userName, final String password)
            throws MalformedURLException, ServiceException, RemoteException;

    String loginToSoapService() throws MalformedURLException, ServiceException, RemoteException;

    void logoutFromSoapService(final String authenticationToken);

    void updateLicense() throws IOException;

    void refreshLicense();

    void installTestLibrary();

    /**
     * Install the plugin referenced by {@link TesterConfiguration#getPluginJar()}.  
     * This is only done once per integration test run.
     * This method does nothing if the plugin is already installed <em>via this method</em> or when
     * {@link TesterConfiguration#getPluginJar()} returns <tt>null</tt>.
     */
    void installPlugin();

    /**
     * Install a plugin into Confluence. This is only done once per integration test run. This method is a
     * no-op if the method has been called before with the same plugin.
     * @param pluginJar
     * The plugin JAR
     */
    void installPlugin(File pluginJar);

    /**
     * Restore the class path resource &quot;site-export.zip&quot; into Confluence.
     * @deprecated
     * Please use {@link #restoreData(java.io.File)} instead.
     */
    void restoreData();

    /**
     * Restore an export into Confluence.
     * @param exportZip
     * The export ZIP file
     */
    void restoreData(File exportZip);

    void flushCaches();

    /**
     * Asserts that a resource is XSRF protected.
     *
     * @param resourcePath The resource to test for XSRF protection. It must not contain the XSRF token. Must be absolute (e.g. <tt>/admin/plugins.action</tt>).
     */
    void assertResourceXsrfProtected(String resourcePath);

    void assertXsrfTokenNotPresentFailure(String resourcePath);

    void assertXsrfTokenNotValidFailure(String resourcePath);

    boolean gotoPageWithEscalatedPrivileges(String destination);

    boolean gotoPageWithEscalatedPrivileges(String destination, String urlEncoding);

    void dropEscalatedPrivileges();
}
