package com.atlassian.confluence.plugin.functest.module.xmlrpc.index;

import com.atlassian.confluence.rpc.SecureRpc;

public interface IndexHelperService extends SecureRpc
{
    Boolean reindex(final String authenticationToken);

    Boolean flush(final String authenticationToken);

}
