package com.atlassian.confluence.plugin.functest.module.xmlrpc.page;


import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import org.apache.commons.lang.StringUtils;

import java.util.Hashtable;
import java.util.Map;

public class PageHelperServiceDelegate implements PageHelperService
{
    private PermissionManager permissionManager;

    private PageManager pageManager;

    @SuppressWarnings("unused")
    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    @SuppressWarnings("unused")
    public void setPageManager(PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

    @Override
    public Map<String, ?> getPage(String authenticationToken, String pageId)
    {
        Page page = pageManager.getPage(Long.parseLong(pageId));
        Map<String, Object> pageStruct = null;

        if (null != page && permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(),  Permission.VIEW, page))
        {
            pageStruct = new Hashtable<String, Object>();

            pageStruct.put("id", page.getIdAsString());
            pageStruct.put("spaceKey", page.getSpaceKey());

            Page parentPage = page.getParent();
            if (null != parentPage)
                pageStruct.put("parentId", page.getParent().getIdAsString());

            pageStruct.put("title", page.getTitle());
            pageStruct.put("version", page.getVersion());
            pageStruct.put("content", page.getBodyAsString());

            if (StringUtils.isNotBlank(page.getCreatorName()))
                pageStruct.put("creator", page.getCreatorName());

            pageStruct.put("created", page.getCreationDate());

            if (StringUtils.isNotBlank(page.getLastModifierName()))
                pageStruct.put("lastModifier", page.getLastModifierName());

            pageStruct.put("lastModified", page.getLastModificationDate());
        }

        return pageStruct;
    }

    @Override
    public String login(String s, String s1) throws RemoteException
    {
        return null;
    }

    @Override
    public boolean logout(String s) throws RemoteException
    {
        return false;
    }
}
