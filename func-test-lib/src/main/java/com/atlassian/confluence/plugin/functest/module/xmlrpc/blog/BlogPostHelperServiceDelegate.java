package com.atlassian.confluence.plugin.functest.module.xmlrpc.blog;

import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.user.UserPreferencesKeys;
import com.atlassian.user.User;
import org.apache.commons.lang.StringUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.TimeZone;

public class BlogPostHelperServiceDelegate implements BlogPostHelperService {

    private PageManager pageManager;

    private PermissionManager permissionManager;

    private UserAccessor userAccessor;

    public PageManager getPageManager() {
        return pageManager;
    }

    public void setPageManager(PageManager pageManager) {
        this.pageManager = pageManager;
    }

    public PermissionManager getPermissionManager() {
        return permissionManager;
    }

    public void setPermissionManager(PermissionManager permissionManager) {
        this.permissionManager = permissionManager;
    }

    public UserAccessor getUserAccessor() {
        return userAccessor;
    }

    public void setUserAccessor(UserAccessor userAccessor) {
        this.userAccessor = userAccessor;
    }

    public String login(String s, String s1) throws RemoteException {
        return null;
    }

    public boolean logout(String s) throws RemoteException {
        return false;
    }

    private TimeZone getUserTimeZone() {
        final User user = AuthenticatedUserThreadLocal.getUser();

        if (null != user) {
            final String timeZoneId =
                    getUserAccessor().getUserPreferences(user).getString(
                            UserPreferencesKeys.PROPERTY_USER_TIME_ZONE);

            if (StringUtils.isNotBlank(timeZoneId))
                return TimeZone.getTimeZone(timeZoneId);
        }
        
        return TimeZone.getDefault();
    }

    public String getBlogPostId(
            final String authenticationToken,
            final String spaceKey,
            final String title,
            final Date day) throws RemoteException {
        final Calendar _day;
        final BlogPost blogPost;

        if (StringUtils.isBlank(spaceKey))
            throw new RemoteException("Space key not specified.");

        if (StringUtils.isBlank(title))
            throw new RemoteException("Blog post title not specified.");

        if (null == day)
            throw new RemoteException("Blog post publish date not specified.");

        _day = Calendar.getInstance(getUserTimeZone());
        _day.setTime(day);

        blogPost = getPageManager().getBlogPost(spaceKey, title, _day);

        return (null != blogPost && getPermissionManager().hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, blogPost))
                ? blogPost.getIdAsString()
                : null;
    }

    @Override
    public Map<String, ?> getBlogPost(String authenticationToken, String id)
    {
        Map<String, Object> postStructure = null;
        
        BlogPost post = getPageManager().getBlogPost(Long.parseLong(id));
        if (null != post && permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, post))
        {
            postStructure = new Hashtable<String, Object>();
            postStructure.put("id", post.getIdAsString());
            postStructure.put("spaceKey", post.getSpaceKey());
            postStructure.put("title", post.getTitle());
            postStructure.put("version", post.getVersion());
            postStructure.put("content", post.getBodyAsString());
            
            if (StringUtils.isNotBlank(post.getCreatorName()))
                postStructure.put("creator", post.getCreatorName());

            postStructure.put("created", post.getCreationDate());
        }

        return postStructure;
    }
}
