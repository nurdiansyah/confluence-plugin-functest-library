package com.atlassian.confluence.plugin.functest.module.xmlrpc.comment;


import com.atlassian.confluence.rpc.SecureRpc;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public interface CommentHelperService extends SecureRpc
{
    Map<String, ?> getComment(String authenticationToken, String commentId);

    List<String> getCommentIds(final String authenticationToken, String pageId);

    String addOrUpdateComment(final String authenticationToken, Hashtable<String, ?> commentStruct);
}
