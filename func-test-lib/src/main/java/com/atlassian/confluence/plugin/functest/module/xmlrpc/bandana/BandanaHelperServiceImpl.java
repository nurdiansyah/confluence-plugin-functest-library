package com.atlassian.confluence.plugin.functest.module.xmlrpc.bandana;

import com.atlassian.confluence.rpc.RemoteException;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Map;


public class BandanaHelperServiceImpl implements BandanaHelperService
{
    private BandanaHelperService bandanaHelperService;

    private PlatformTransactionManager transactionManager;

    public void setBandanaHelperService(BandanaHelperService bandanaHelperService)
    {
        this.bandanaHelperService = bandanaHelperService;
    }

    public void setTransactionManager(PlatformTransactionManager transactionManager)
    {
        this.transactionManager = transactionManager;
    }

    public Boolean setValue(final String authenticationToken, final String spaceKey, final String key, final String value)
    {
        return (Boolean) new TransactionTemplate(transactionManager).execute(
                new TransactionCallback()
                {
                    public Object doInTransaction(TransactionStatus transactionStatus)
                    {
                        try
                        {
                            return bandanaHelperService.setValue(authenticationToken, spaceKey, key, value);
                        }
                        catch (RemoteException re)
                        {
                            throw new RuntimeException(re);
                        }
                    }
                }
        );
    }

    public Boolean removeValue(final String authenticationToken, final String spaceKey, final String key)
    {
        return (Boolean) new TransactionTemplate(transactionManager).execute(
                new TransactionCallback()
                {
                    public Object doInTransaction(TransactionStatus transactionStatus)
                    {
                        try
                        {
                            return bandanaHelperService.removeValue(authenticationToken, spaceKey, key);
                        }
                        catch (RemoteException re)
                        {
                            throw new RuntimeException(re);
                        }
                    }
                }
        );
    }


    @SuppressWarnings("unchecked")
    public Map<String, Object> getValue(final String authenticationToken, final String spaceKey, final String key)
    {
        return (Map<String, Object>) new TransactionTemplate(transactionManager).execute(
                new TransactionCallback()
                {
                    public Object doInTransaction(TransactionStatus transactionStatus)
                    {
                        try
                        {
                            return bandanaHelperService.getValue(authenticationToken, spaceKey, key);
                        }
                        catch (RemoteException re)
                        {
                            throw new RuntimeException(re);
                        }
                    }
                }
        );
    }

    public String login(String s, String s1)
    {
        return null;
    }

    public boolean logout(String s)
    {
        return false;
    }
}
